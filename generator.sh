#!/bin/sh

echo ""
echo Let\'s build a snowman!
echo ""

echo What should we call your snowman \(e.g. tom, george, beowulf\)?

read name

if [[ "$name" == "" ]]; then
  name="beowulf"
fi

echo Your snowman will be named $name
echo Where should $name live \(e.g.  ./$name\)?
echo ""

read path

if [[ "$path" == "" ]]; then
  path=./$name
fi

git clone "https://gitlab.com/fresheyeball/Shpadoinkle-snowman" "$path" || exit 1

echo Naming snowman $name...
echo ""

# sed differs slightly between linux and osx
# https://ed.gs/2016/01/26/os-x-sed-invalid-command-code/
# https://stackoverflow.com/a/57766728
xOsReplace() {
  if [[ "$OSTYPE" == "darwin"* ]]; then
    xargs -0 sed -i '' -e "s/snowman/$name/g"
  else
    xargs -0 sed -i -e "s/snowman/$name/g"
  fi
}

# Find and replace snowman in all files except the .git directory and README.md
find "$path" \( -type d -name .git -prune \) -o -type f ! -name README.md -print0 | xOsReplace

mv "$path/snowman.cabal" "$path/$name.cabal"

echo "Sweeping up..."
echo ""
rm -rf $path/.git
rm $path/README.md
mv $path/README.md.gen $path/README.md
rm $path/*.sh
rm $path/.gitlab-ci.yml

cat $path/success.gen
rm $path/success.gen

cd $path
