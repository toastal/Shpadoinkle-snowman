{-# LANGUAGE OverloadedStrings #-}


module Main where


import           Shpadoinkle
import           Shpadoinkle.Backend.ParDiff
import           Shpadoinkle.Html


view :: () -> Html m ()
view _ = "hello world"


main :: IO ()
main = do
  putStrLn "\nHappy point of view on https://localhost:8080\n"
  runJSorWarp 8080 $ simple runParDiff () view getBody
